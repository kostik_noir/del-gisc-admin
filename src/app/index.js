import React from 'react';
import NewUser from '../users/new-user';

function App() {
  return (
    <div className="app">
      <NewUser/>
    </div>
  );
}

export default App;
