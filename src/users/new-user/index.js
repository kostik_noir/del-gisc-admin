import React, { useState } from 'react';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import NewUserForm from './new-user-form';

export default function NewUser() {
  const [isDialogVisible, setDialogVisibility] = useState(false);

  const onAddBtnPressed = () => {
    setDialogVisibility(true);
  };

  const hideDialog = () => {
    setDialogVisibility(false);
  };

  return (
    <>
      <Button label='Создать' icon="pi pi-plus" badgeClassName='p-button-success' onClick={ onAddBtnPressed }/>
      <Dialog visible={ isDialogVisible }
              draggable={ false }
              style={ { width: '30vw' } }
              header='Новый пользователь'
              onHide={ hideDialog }>
        <NewUserForm onSubmitted={ hideDialog } onCancelled={ hideDialog }/>
      </Dialog>
    </>
  );
}
