import React from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { classNames } from 'primereact/utils';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import styles from './styles.module.scss';

const initialValues = {
  name: '',
  password: '',
  email: '',
  phone: '',
  skype: ''
};

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Слишком короткое имя')
    .max(50, 'Слишком длинное имя')
    .required('Введите имя пользователя'),
  password: Yup.string()
    .min(8, 'Не меньше 8 символов')
    .required('Введите пароль'),
  email: Yup.string()
    .email('Неправильное значение email')
    .required('Введите email'),
  phone: Yup.string()
    .required('Введите номер телефона'),
  skype: Yup.string()
    .required('Введите Skype ID')
});

export default function NewUserForm({ onSubmitted, onCancelled }) {
  const onSubmit = (values) => {
    alert(JSON.stringify(values, null, 2));
    onSubmitted();
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit
  });

  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);

  const getFormErrorMessage = name =>
    isFormFieldValid(name) && <small className="p-error">{ formik.errors[name] }</small>;

  const createTextInput = ({ fieldName, fieldType = 'text', label }) => (
    <>
      <span className="p-float-label">
        <InputText id={ fieldName }
                   name={ fieldName }
                   type={ fieldType }
                   { ...formik.getFieldProps(fieldName) }
                   className={ classNames({ 'p-invalid': isFormFieldValid(fieldName) }) }
        />
        <label htmlFor={ fieldName }
               className={ classNames({ 'p-error': isFormFieldValid(fieldName) }) }>{ label }</label>
      </span>
      { getFormErrorMessage(fieldName) }
    </>
  );

  return (
    <form onSubmit={ formik.handleSubmit } className={ styles.form }>
      <div className='p-fluid'>
        <div className="p-field">
          { createTextInput({ fieldName: 'name', label: 'Имя*' }) }
        </div>

        <div className="p-field">
          { createTextInput({ fieldName: 'password', label: 'Пароль*', fieldType: 'password' }) }
        </div>

        <div className="p-field">
          { createTextInput({ fieldName: 'email', label: 'E-mail*', fieldType: 'email' }) }
        </div>

        <div className="p-field">
          { createTextInput({ fieldName: 'phone', label: 'Телефон*', fieldType: 'tel' }) }
        </div>

        <div className="p-field">
          { createTextInput({ fieldName: 'skype', label: 'Skype*' }) }
        </div>
      </div>

      <div className={ classNames(styles.formFooter) }>
        <Button type='button'
                label='Отмена'
                onClick={ onCancelled }
                className={ classNames(styles.btn, 'p-button-text', 'p-button-secondary') }/>
        <Button type='submit'
                label='Создать'
                className={ classNames(styles.btn) }/>
      </div>

    </form>
  );
}
